package Task9_1;


import com.github.javafaker.Faker;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Faker faker = new Faker();

        Human[] candidateArrays = new Human[10];

        for (int i = 0; i < candidateArrays.length; i++){
            candidateArrays[i] = new Human(100+i, faker.name().fullName(), faker.number().numberBetween(18, 55));
        }
        System.out.println(Arrays.toString(candidateArrays));
        System.out.println("----------------------");

        Arrays.stream(candidateArrays)
                .limit(8)
                .sorted((o1, o2) -> o2.age - o1.age)
                .filter(s -> s.age > 20)
                .filter(s -> s.age < 45)
                .peek(System.out::println)
                .map(o -> new Astronaut(o.name, o.age, "Earth"))
                .sorted(Comparator.comparing(o -> o.name))
                .toList()
                .forEach(System.out::println);
    }
 }
