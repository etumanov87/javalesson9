package Task9_1;

public class Human {
    int id = 0;

    String name;

    int age;

    public Human(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;

    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
