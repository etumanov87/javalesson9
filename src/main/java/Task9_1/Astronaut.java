package Task9_1;

public class Astronaut {
    String name;

    int age;

    String planetDispatches;

    public Astronaut(String name, int age, String planetDispatches) {
        this.name = name;
        this.age = age;
        this.planetDispatches = planetDispatches;
    }

    @Override
    public String toString() {
        return "Astronaut{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", planetDispatches='" + planetDispatches + '\'' +
                '}';
    }
}
