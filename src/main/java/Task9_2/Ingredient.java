package Task9_2;

public enum Ingredient {
    CUCUMBERS ("огурцы"),
    TOMATOES ("помидоры"),
    CUTLET ("котлета"),
    CHEESE ("сыр"),
    KETCHUP ("кетчуп"),
    MAYONNAISE ("майонез"),
    BUN ("булочка"),
    SALAD ("салат");

    Ingredient(String title) {
    }
}
