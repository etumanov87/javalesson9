package Task9_2;

import java.util.List;

public class IngredientPackaging {
    private final List<Ingredient> ingredients;

    public IngredientPackaging(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }
}
