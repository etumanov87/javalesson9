package Task9_2;

import java.util.Collections;
import java.util.List;

public class Chief {
    public boolean checkIngredients(IngredientPackaging packaging) {
        long bunCount = packaging
                .getIngredients()
                .stream()
                .filter(ingredient -> ingredient == Ingredient.BUN)
                .count();
        long cutletCount = packaging
                .getIngredients()
                .stream()
                .filter(ingredient -> ingredient == Ingredient.CUTLET)
                .count();
        return bunCount >= 2 && cutletCount >= 1;
    }

    public Burger cookBurger(IngredientPackaging packaging) {
        List<Ingredient> ingredients = packaging.getIngredients();

        Collections.swap(ingredients,
                0,
                ingredients.indexOf(Ingredient.BUN));

        Collections.swap(ingredients,
                ingredients.size() - 1,
                ingredients.lastIndexOf(Ingredient.BUN));

        for (Ingredient ingredient : ingredients) {
            if (ingredient == Ingredient.CHEESE) {
                Collections.swap(ingredients,
                        ingredients.indexOf(Ingredient.CUTLET),
                        ingredients.indexOf(Ingredient.CHEESE));
            }
        }


        for (Ingredient ingredient : ingredients) {
            if (ingredient == Ingredient.SALAD) {
                Collections.swap(ingredients,
                        ingredients.size() - 2,
                        ingredients.indexOf(Ingredient.SALAD));
                break;
            }
        }

        if (ingredients
                .stream()
                .filter(ing -> ing == Ingredient.BUN)
                .count() == 3) {
            Collections.swap(ingredients.subList(1, ingredients.size()),
                    ingredients.size() / 2,
                    ingredients.subList(1, ingredients.size()).indexOf(Ingredient.BUN));
        }
        return new Burger(ingredients);
    }
}
