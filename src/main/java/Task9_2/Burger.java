package Task9_2;

import java.util.List;

public class Burger {
    private final List<Ingredient> ingredients;
    private final long cookingTime;

    public Burger(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        this.cookingTime = System.currentTimeMillis();
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getCookingTime() {
        long seconds = (cookingTime / 1000) % 60;
        long minutes = (cookingTime / (1000 * 60)) % 60;
        long hours = (cookingTime / (1000 * 3600)) % 24;
        return String.format("%d часов, %d минут, %d секунд", hours + 3 , minutes, seconds);/*cookingTime / 60000;*/
    }
}
