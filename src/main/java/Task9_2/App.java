package Task9_2;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static Task9_2.Ingredient.*;

public class App {
    public static void main(String[] args) {
        Chief chef = new Chief();
        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            List<Ingredient> randomIngredients = Arrays.asList(
                    Ingredient.values()[random.nextInt(8)],
                    Ingredient.values()[random.nextInt(8)],
                    Ingredient.values()[random.nextInt(8)],
                    Ingredient.values()[random.nextInt(8)],
                    Ingredient.values()[random.nextInt(8)],
                    Ingredient.values()[random.nextInt(8)]
             );
            IngredientPackaging packaging = new IngredientPackaging(randomIngredients);

            if (chef.checkIngredients(packaging)) {
                Burger burger = chef.cookBurger(packaging);
                System.out.println("Бургер приготовлен в " + burger.getCookingTime() + " из ингредиентов: " + burger.getIngredients());
            }
        }
    }
}
